'use strict'

const chai = require('chai')
const expect = require('chai').expect
const should = chai.should()
const chaiHttp = require('chai-http')
const server = require('../server')
const User = require('../model/user')
const constants = require('../lib/constants')
const crypto = require('crypto')
const config = require('../config/config.js')

chai.use(chaiHttp)
let userTest
describe('desafio_node_rafael_santos - authlib', function() {
    describe('Validações Genéricas', function() {
        it('Mock Record - check database', function(done) {
            User.collection.remove({})
            userTest = new User({nome: 'manuel',
                email: 'manuel.santos@concretesolutions.com.br',
                senha: crypto.createHmac('sha1',
                            config.key.privateKey)
                            .update('99999')
                            .digest('hex'),
                telefones: [{numero: 999999999, ddd: 99}]})

            userTest.validate(function(err) {
                expect(err.errors.repost).to.not.exist
                done()
            })
        })

        it('Somente request JSON são aceitas',
           function(done) {
               chai.request(server)
                .post('/signup')
                .set('content-type', 'application/html')
                .end(function(err, res) {
                    res.should.have.status(constants.REQUEST_URL_NOT_FOUND)
                    done()
                })
           })

        it('Sessão inválida / Nenhuma rota definida para atender a requisição.',
               function(done) {
                   chai.request(server)
                       .post('/blabla')
                       .set('content-type', 'application/json')
                       .end(function(err, res) {
                           res.should.have.status(500)
                           done()
                       })
               })
    })
    describe('Inscrição - /signup POST', function() {
        it('Request sem nome - /signup POST', function(done) {
            chai.request(server)
                  .post('/signup')
                  .send({
                      email: 'manuel.santos@concretesolutions.com.br',
                      telefones: [{numero: 999999, ddd: 21}]})
                  .set('content-type', 'application/json')
                  .end(function(err, res) {
                      res.should.have.status(constants.BAD_REQUEST)
                  })
            done()
        })

        it('Request sem senha - /signup POST', function(done) {
            chai.request(server)
                      .post('/signup')
                      .send({
                          nome: 'rafael',
                          email: 'manuel.santos@concretesolutions.com.br'})
                    .set('content-type', 'application/json')
                    .end(function(err, res) {
                        res.should.have.status(constants.BAD_REQUEST)
                    })
            done()
        })

        it('Inscrição Ok - /signup POST', function(done) {
            chai.request(server)
                   .post('/signup')
                   .send({'nome': 'rafael',
                       'email': 'rafael.santos@concretesolutions.com.br',
                       'senha': '99999',
                       'telefones': [{'numero': 999999999, 'ddd': 99}]})
                    .set('content-type', 'application/json')
                    .end(function(err, res) {
                        res.should.have.status(constants.OK)
                        res.should.be.json
                        res.body.should.be.a('object')
                        res.body.should.have.property('id')
                        res.body.should.have.property('nome').equal('rafael')
                        res.body.should.have.property('email')
                          .equal('rafael.santos@concretesolutions.com.br')
                        res.body.should.have.property('senha')
                        .equal(crypto.createHmac('sha1',
                        config.key.privateKey)
                        .update('99999')
                        .digest('hex'))
                        res.body.telefones[0].should.have.property('numero')
                          .equal(999999999)
                        res.body.telefones[0].should.have.property('ddd')
                        .equal(99)
                    })
            done()
        })
    })
    describe('Atualização inscrição - /signin POST', function() {
        it('Email inválido - /signin POST', function(done) {
            chai.request(server)
                .get('/signin')
                .send({email: 'x',
                    senha: userTest.senha})
                .set('content-type', 'application/json')
                .end(function(err, res) {
                    res.should.have.status(constants.BAD_REQUEST)
                    res.should.be.json
                    res.body.should.be.a('object')
                    res.body.should.have.property('mensagem')
                })
            done()
        })

        it('Senha inválida - /signin POST', function(done) {
            chai.request(server)
                .get('/signin')
                .send({email: userTest.email,
                    senha: 'x'})
                .set('content-type', 'application/json')
                .end(function(err, res) {
                    should.exist(err)
                    res.should.have.status(constants.BAD_REQUEST)
                    res.should.be.json
                    res.body.should.be.a('object')
                    res.body.should.have.property('mensagem')
                })
            done()
        })
        it('Atualização inscrição Ok - /signin POST', function(done) {
            chai.request(server)
                .get('/signin')
                .send({email: userTest.email,
                    senha: userTest.senha})
                .set('content-type', 'application/json')
                .end(function(err, res) {
                    res.should.have.status(constants.OK)
                    res.should.be.json
                    res.body.should.be.a('object')
                    res.body.should.have.property('id')
                    res.body.should.have.property('nome').equal('manuel')
                    res.body.should.have.property('email')
                      .equal('manuel.santos@concretesolutions.com.br')
                    res.body.should.have.property('senha').equal('99999')
                    res.body.telefones[0].should.have.property('numero')
                      .equal(999999999)
                    res.body.telefones[0].should.have.property('ddd').equal(99)
                    res.body.token.should.property('token')
                    res.body.createdAt.should.property('createdAt')
                    res.body.updatedAt.should.property('updatedAt')
                })
            done()
        })
    })
    describe('Buscando inscrição - /signin POST', function() {
        it('Token inválido - /find GET', function(done) {
            chai.request(server)
                .get('/find/' + userTest.id)
                .set('Authorization', 'Bearer ' + 0)
                .set('content-type', 'application/json')
                .end(function(err, res) {
                    res.should.have.status(constants.BAD_REQUEST)
                    res.should.be.json
                    res.body.should.be.a('object')
                    res.body.should.have.property('mensagem')
                })
            done()
        })
        it('Id inválido - /find GET', function(done) {
            chai.request(server)
                .get('/find/' + userTest.id)
                .set('Authorization', 'Bearer ' + 0)
                .set('content-type', 'application/json')
                .end(function(err, res) {
                    res.should.have.status(constants.BAD_REQUEST)
                    res.should.be.json
                    res.body.should.be.a('object')
                    res.body.should.have.property('mensagem')
                })
            done()
        })
        it('Buscando inscrição Ok - /find GET', function(done) {
            chai.request(server)
                .get('/find/' + userTest.id)
                .set('Authorization', 'Bearer ' + userTest.token)
                .set('content-type', 'application/json')
                .end(function(err, res) {
                    res.should.have.status(constants.OK)
                    res.should.be.json
                    res.body.should.be.a('object')
                    res.body.should.have.property('id')
                    res.body.should.have.property('nome').equal('manuel')
                    res.body.should.have.property('email')
                      .equal('manuel.santos@concretesolutions.com.br')
                    res.body.should.have.property('senha').equal('99999')
                    res.body.telefones[0].should.have.property('numero')
                      .equal(999999999)
                    res.body.telefones[0].should.have.property('ddd').equal(99)
                    res.body.token.should.property('token')
                    res.body.createdAt.should.property('createdAt')
                    res.body.updatedAt.should.property('updatedAt')
                })
            done()
        })
    })
})
