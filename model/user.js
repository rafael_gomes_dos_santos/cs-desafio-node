'use strict'
const mongoose = require('mongoose')
const config = require('../config/config')
const constants = require('../lib/constants')
const crypto = require('crypto')

mongoose.connect('mongodb://' + config.database.host + '/' + config.database.db)
const db = mongoose.connection

db.on('error', function (err) {
    console.log('Mongoose default connection error: ' + err)
})

db.once('open', function callback() {
    console.log('Connection with database succeeded.')
})

const userSchema = new mongoose.Schema({
    nome: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    senha: {type: String, required: true},
    telefones: [{numero: Number, ddd: Number}],
    token: {type: String, required: true, unique: true},
    ultimo_login: {type: Date, required: true},
}, {timestamps: {}})

userSchema.methods.signup = function(callback) {
    this.ultimo_login = Date.now()
    this.save(function(err, user) {
        let result
        if(user) {
            result = user.toObject()
            result.id = user._id
            delete result._id
            delete result.__v
        }
        callback(err, result)
    })
}

userSchema.methods.signin = function(callback) {
    if(!this.email || !this.senha || !this.token) {
        callback(constants.INVALID_REQUEST)
        return
    }
    this.ultimo_login = Date.now()
    User.findOneAndUpdate(
        {email: this.email,
            senha: crypto.createHmac('sha1',
         config.key.privateKey)
         .update(this.senha)
         .digest('hex')},
        {ultimo_login: this.ultimo_login, token: this.token},
        {new: true}, function(err, user) {
            let result
            if(user) {
                result = user.toObject()
                result.id = user._id
                delete result._id
                delete result.__v
            }
            callback(err, result)
        })
}

userSchema.methods.find = function(callback) {
    if(!this._id || !this.token) {
        callback(constants.INVALID_REQUEST)
        return
    }

    const token = this.token

    User.findById(this._id, function(err, user) {
        if (err) throw err
        if(user) {
            if(user.token == token) {
                const result = user.toObject()
                result.id = user._id
                delete result._id
                delete result.__v
                callback(null, result)
            }else{
                callback(constants.NOT_AUTHORIZED)
            }
        }else{
            callback(constants.NOT_AUTHORIZED)
        }
    })
}

const User = mongoose.model('User', userSchema)

module.exports = User
