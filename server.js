'use strict'
const express = require('express')
const path = require('path')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const user = require('./model/user')
const util = require('util')
const constants = require('./lib/constants')
const config = require('./config/config.js')
const expressJWT = require('express-jwt')
const routes = require('./routes/routes.js')

const server = express()

// view engine setup
server.set('views', path.join(__dirname, 'views'))
server.set('view engine', 'jade')

server.use(logger('dev'))
server.use(bodyParser.json())
server.use(bodyParser.urlencoded({extended: true}))
server.use(cookieParser())
server.use(express.static(path.join(__dirname, 'public')))
server.use(expressJWT({
    secret: config.key.privateKey}).unless( {
        path: ['/signup', '/signin']}))

server.use(function(req, res, next) {
    const contype = req.headers['content-type']
    if(!contype || contype.indexOf('application/json')!== 0)
        return res.status(constants.REQUEST_URL_NOT_FOUND).json(
            util.format(constants.ERRMSG, constants.ACCEPT_ONLY_JSON)
        )
    else req.database = user

    next()
})

// *** main routes *** //
server.use('/', routes)

server.use(function(err, req, res, next) {
    res.status(constants.REQUEST_URL_NOT_FOUND).json(
        util.format(constants.ERRMSG, constants.NOT_FOUND_ENDPOINT)
    )
    next(err)
})

// error handlers

server.use( function(err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        if(err.message == 'jwt expired') {
            res.status(constants.UNAUTHORIZED)
            .json({mensagem: 'Invalid session'})
        } else if(err.message == 'invalid token' ||
                err.message == 'No authorization token was found') {
            res.status(constants.UNAUTHORIZED)
                      .json(util.format(constants.ERRMSG,	constants.NOT_AUTHORIZED))
        }else {
            res.status(constants.UNAUTHORIZED)
             .json(util.format(constants.ERRMSG,	constants.NOT_AUTHORIZED))
        }
    }
    next(err)
})

// development error handler
// will print stacktrace
if (server.get('env') === 'development') {
    server.use(function(err, req, res, next) {
        res.status(err.status || 500)
        res.render('error', {message: err.message, error: err})
        next(err)
    })

}

// production error handler
// no stacktraces leaked to user
server.use(function(err, req, res, next) {
    res.status(err.status || 500)
    res.render('error', {message: err.message, error: {}})
    next(err)
})

module.exports = server
