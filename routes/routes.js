const express = require('express')
const router = new express.Router()
const authlib = require('../lib/authlib')

router.post('/signup', authlib.signup)
router.post('/signin', authlib.signin)
router.get('/find/:id', authlib.find)

module.exports = router
