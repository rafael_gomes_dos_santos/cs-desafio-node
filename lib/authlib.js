'use strict'
const User = require('../model/user.js')
const config = require('../config/config.js')
const jwt = require('jsonwebtoken')
const util = require('util')
const constants = require('../lib/constants')
const crypto = require('crypto')


/**
*
*
*
*/

const signup = function(req, res) {
    jwt.sign(
        {email: req.body.email},
        config.key.privateKey,
        {expiresIn: config.key.tokenExpiry},
        function(err, token) {
            if(err) {
                res.status(constants.BAD_REQUEST)
                    .json(util.format(constants.FOUND, err))
            }else {
                req.body.token = token
                req.body.senha = crypto.createHmac('sha1',
                    config.key.privateKey).update(req.body.senha).digest('hex')
                const user = new User(req.body)
                const callback = function(err, resp) {
                    if(err) {
                        if (err.code == constants.DUPLICATE_KEY) {
                            res.status(constants.BAD_REQUEST).json(
                                util.format(constants.FOUND, user.email))
                        }else {
                            res.status(constants.BAD_REQUEST).json(
                                util.format(constants.ERRMSG, user.message))
                        }
                    }else {
                        res.status(constants.OK).json(resp)
                    }
                }
                user.signup(callback)
            }
        }
    )
    return
}

/**
*
*
*
*/

const signin = function(req, res) {
    jwt.sign(
        {email: req.body.email},
        config.key.privateKey,
        {expiresIn: config.key.tokenExpiry},
        function(err, token) {
            if(err) {
                res.status(constants.BAD_REQUEST)
                    .json(util.format(constants.ERRMSG, err))
            }else {
                req.body.token = token
                const user = new User(req.body)
                const callback = function(err, resp) {
                    if(err) {
                        res.status(constants.UNAUTHORIZED).json(
                            util.format(constants.ERRMSG,	err))
                    }else{
                        if (!resp) {
                            res.status(constants.UNAUTHORIZED).json(
                                util.format(constants.NOTFOUND, user.email))
                        }else {
                            res.status(constants.OK).json(resp)
                        }
                    }
                }
                user.signin(callback)
            }
        })
    return
}

/**
*
*
*
*/

const find = function(req, res) {
    let token = req.headers.authorization
    const id = req.params.id
    token = token.replace('Bearer ', '')
    jwt.verify(token, config.key.privateKey, function(err, token) {

        if (err) {
            res.status(constants.UNAUTHORIZED)
                .json(util.format(constants.ERRMSG, err))
        } else {
            const user = new User({_id: id, token: token})
            const callback = function(err, resp) {
                if(err) {
                    res.status(constants.UNAUTHORIZED).json(
                        util.format(constants.ERRMSG,	err))
                }else {
                    res.status(constants.OK).json(resp)
                }
            }
            user.find(callback)
        }
    })
}

/**
*
*
*
*/

module.exports = {
    signup: signup,
    signin: signin,
    find: find}
